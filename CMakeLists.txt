cmake_minimum_required(VERSION 3.14)

project(ParseArguments)
add_executable(ParseArguments main.cpp argumentparser.h)

set_target_properties(ParseArguments PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED OFF
    CXX_EXTENSIONS OFF
)


#set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17 -Wall")
#set (CMAKE_CXX_STANDARD 17)

#if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
#    set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -std=c++17 -W -Wall -pedantic")
#endif()

#[[
set(SOURCE
    main.cpp
    parsearguments.h
)

add_executable(${PROJECT_NAME} ${SOURCE})
]]

#[[
add Google Test ass submodule
https://cliutils.gitlab.io/modern-cmake/chapters/testing/googletest.html
]]

#[[
option(PACKAGE_TESTS "Build the tests" ON)
if (PACKAGE_TESTS)
    enable_testing()
    add_subdirectory(tests)
endif()

add_subdirectory("${PROJECT_SOURCE_DIR}/extern/googletest" "extern/googletest")

mark_as_advanced(
    BUILD_GMOCK BUILD_GTEST BUILD_SHARED_LIBS
    gmock_build_tests gtest_build_samples gtest_build_tests
    gtest_disable_pthreads gtest_force_shared_crt gtest_hide_internal_symbols
)

set_target_properties(gtest PROPERTIES FOLDER extern)
set_target_properties(gtest_main PROPERTIES FOLDER extern)
set_target_properties(gmock PROPERTIES FOLDER extern)
set_target_properties(gmock_main PROPERTIES FOLDER extern)

#macro(package_add_test TESTNAME)
#    add_executable(${TESTNAME} ${ARGN})
#    target_link_libraries(${TESTNAME} gtest gmock gtest_main)
#    add_test(NAME ${TESTNAME} COMMAND ${TESTNAME})
#    set_target_properties(${TESTNAME} PROPERTIES FOLDER tests)
#endmacro()

#package_add_test(test1 tests/test1.cpp)
]]

