#include "gtest/gtest.h"
#include "../parsearguments.h"

TEST(RUNTEST, Positive) {
    EXPECT_EQ(1, 1);
}

TEST(RUNTEST, Negative) {
    EXPECT_EQ(1, 0);
}


TEST(INIT, Initialisation) {
    ParseArguments parser("ribotools", 1.01);
    parser.addArgumentPositional("task").setHelp("given subtask").setDefaultValue(true);
    bool h = parser.get<bool>("task");
    EXPECT_EQ(h, true);
}

