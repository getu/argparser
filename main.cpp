#include<iostream>

#include "argumentparser.h"

int main(const int argc, const char *argv[])
{
    bool testFlag;
    int dim;

    auto p = ArgumentParser();
    p.setDescription("test argument parser code");
    p.addArgumentCommand("mode").setHelp("use mode settings");
    p.addArgumentCommand("movie").setHelp("play movie");
    p.addArgumentCommand("image").setHelp("show image");
    p.addArgumentPositional("file").setHelp("protein coding fasta file");
    p.addArgumentRequired("dimension").setKeyShort("-d").setKeyLong("--dim").setDefaultValue<int>(100);
    p.addArgumentOptional("width").setKeyShort("-w").setKeyLong("--width").setDefaultValue<double>(2.0);
    p.addArgumentFlag("test mode").setKeyShort("-t").setKeyLong("--test");
    p.addArgumentFlag("help message").setKeyShort("-h").setKeyLong("--help");
    p.addArgumentFlag("version tag").setKeyShort("-v").setKeyLong("--version");

    try {
        p.parse(argc, argv);
        testFlag = p.get<bool>("test mode");
        dim = p.get<int>("dimension");
    }
    catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }


    std::cout << "name " << p.name() << std::endl;
    std::cout << "test flags " << testFlag << std::endl;
    std::cout << "dimension " << dim << std::endl;

    return 0;
}
